#!/usr/bin/env python

"""A basic API server for the Meekan exercise."""

from __future__ import division
import os.path

import tornado.ioloop
import tornado.web
import tornado.options
import redis

from tornado.options import define, options
define("port", default=8000, help="run on the given port", type=int)

redis_client = redis.Redis(host='localhost', db=0)


class TotalHandler(tornado.web.RequestHandler):
    def get(self):
        # Calculate average
        total_sum = sum([int(v) for v in redis_client.hgetall('sum').values()])
        total_count = sum([int(v) for v in
                           redis_client.hgetall('count').values()])
        total_avg = total_sum / total_count
        self.write(str(total_avg))


class UserHandler(tornado.web.RequestHandler):
    def get(self, slack_username):
        pipe = redis_client.pipeline()
        pipe.hget('sum', slack_username)
        pipe.hget('count', slack_username)
        user_sum, user_count = pipe.execute()

        # Calculate the average for the user
        avg_value = int(user_sum) / int(user_count)
        self.write(str(avg_value))


def make_app():
    return tornado.web.Application([
        (r"/average", TotalHandler),
        (r"/average/([^/]+)", UserHandler),
    ])

if __name__ == "__main__":
    app = make_app()
    app.listen(options.port)
    tornado.ioloop.IOLoop.current().start()
